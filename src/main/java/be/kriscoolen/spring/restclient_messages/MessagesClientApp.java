package be.kriscoolen.spring.restclient_messages;

import be.kriscoolen.spring.restclient_messages.client.MessagesClient;
import be.kriscoolen.spring.restclient_messages.domain.Message;
import be.kriscoolen.spring.restclient_messages.domain.MessageList;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@SpringBootApplication
public class MessagesClientApp {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        return builder.requestFactory(HttpComponentsClientHttpRequestFactory::new).build();
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(MessagesClientApp.class,args);
        MessagesClient messagesClient = ctx.getBean(MessagesClient.class);
        //print message with id = 1
        System.out.println("Message with id=1:");
        System.out.println(messagesClient.getMessageById(1));
        System.out.println("All messages:");
        System.out.println(messagesClient.getMessages());
        //create new message
        System.out.println("create new message (author Lisa)");
        URI uriLisa1 = messagesClient.createMessage(new Message("Lisa","I play the sax!"));
        System.out.println("The URI of the new message:"+uriLisa1);
        System.out.println("All messages:");
        System.out.println(messagesClient.getMessages());
        System.out.println("create another new message again with author Lisa");
        URI uriLisa2 = messagesClient.createMessage(new Message("Lisa","I hate Bart!"));
        System.out.println("The URI of the new message:"+uriLisa2);
        System.out.println("All messages of author Lisa:");
        System.out.println(messagesClient.getMessagesByAuthor("Lisa"));
        System.out.println("We update the message with id=3 and change the name to 'Mona Lisa'");
        Message messageWithId3 = messagesClient.getMessageById(3);
        messageWithId3.setAuthor("Mona Lisa");
        messagesClient.updateMessage(messageWithId3);
        System.out.println("All messages:");
        System.out.println(messagesClient.getMessages());
        System.out.println("We change the text of message 3 into 'I really really hate bart!'");
        System.out.println(messagesClient.patchMessage(3,"I really really hate Bart"));
        System.out.println("We delete message with id=3:");
        messagesClient.deleteMessage(3);
        System.out.println("All messages:");
        System.out.println(messagesClient.getMessages());






    }
}

