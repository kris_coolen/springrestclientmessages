package be.kriscoolen.spring.restclient_messages.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
public class MessageList implements Serializable {
    private List<Message> messageList;

    public MessageList(){
    }
    public MessageList(List<Message> messageList){
        this.messageList=messageList;
    }

    @JsonProperty("Messages")
    @XmlElementWrapper(name="Messages")
    @XmlElement(name="Message")
    public List<Message> getMessageList(){
        return messageList;
    }

    public void setMessageList(List<Message> messageList){
        this.messageList=messageList;
    }

    @Override
    public String toString() {
        return "messageList=" + messageList;
    }
}
