package be.kriscoolen.spring.restclient_messages.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@XmlRootElement(name="Message")
@JsonPropertyOrder({"MessageId","MessageAuthor","MessageText"})
public class Message implements Serializable {
    private int id;
    @NotBlank
    private String author;
    @NotBlank
    private String text;

    public Message(){

    }
    public Message(int id, String author, String text){
        this.id=id;
        this.author=author;
        this.text=text;
    }

    public Message(String author,String text){
        this(0,author,text);
    }

    @XmlAttribute(name="MessageId")
    @JsonProperty("MessageId")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlAttribute(name="MessageAuthor")
    @JsonProperty(value="MessageAuthor")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @XmlAttribute(name="MessageText")
    @JsonProperty(value="MessageText")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @XmlTransient
    @JsonIgnore
    public String getDisplayMessage(){
        return author +" : " + text;
    }

    @Override
    public String toString() {
        return "Message[" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", text='" + text + '\'' +
                ']';
    }
}
