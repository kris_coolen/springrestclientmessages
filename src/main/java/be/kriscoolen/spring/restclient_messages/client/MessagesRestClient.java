package be.kriscoolen.spring.restclient_messages.client;

import be.kriscoolen.spring.restclient_messages.domain.Message;
import be.kriscoolen.spring.restclient_messages.domain.MessageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class MessagesRestClient implements MessagesClient {

    private String baseUrl;
    private RestTemplate template;

    @Value("${baseUrl}")
    public void setBaseUrl(String baseUrl){
        this.baseUrl=baseUrl;
    }

    @Autowired
    public void setTemplate(RestTemplate template){
        this.template=template;
    }

    @Override
    public Message getMessageById(int id) {
        ResponseEntity<Message> response = template.getForEntity(baseUrl+"messages/{id}",Message.class,id);
        if(response.getStatusCode()== HttpStatus.OK){
            return response.getBody();
        }
        else return null;
    }

    @Override
    public MessageList getMessages() {
        ResponseEntity<MessageList> response = template.getForEntity(baseUrl+"/messages",MessageList.class);
        if(response.getStatusCode()==HttpStatus.OK){
            return response.getBody();
        }
        else{
            return null;
        }
    }

    @Override
    public MessageList getMessagesByAuthor(String author) {
        ResponseEntity<MessageList> response = template.getForEntity(
                baseUrl+"/messages?author={0}",MessageList.class,author);
        if(response.getStatusCode()==HttpStatus.OK) return response.getBody();
        else return null;
    }

    @Override
    public URI createMessage(Message message) {
        return template.postForLocation(baseUrl+"/messages",message);
    }

    @Override
    public void updateMessage(Message message) {
        template.put(baseUrl+"/messages/{0}",message,message.getId());
    }

    @Override
    public Message patchMessage(int id, String text) {
        Message message=new Message();
        message.setText(text);
        return template.patchForObject(baseUrl+"/messages/{0}",message,Message.class,id);
    }

    @Override
    public void deleteMessage(int id) {
        template.delete(baseUrl+"/messages/{0}",id);
    }
}
