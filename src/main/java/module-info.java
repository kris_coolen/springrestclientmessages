open module RestClientMessages {
    requires spring.core;
    requires spring.context;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires java.sql;
    //REST
    requires spring.web;
    //JAXB
    requires java.xml.bind;
    //Jackson
    requires jackson.annotations;
    //validation
    requires java.validation;
}